const express = require('express');
const bodyParser = require('body-parser');
const {execute, subscribe} = require('graphql');
const {graphqlExpress, graphiqlExpress} = require('apollo-server-express');
const schema = require('./schema');
const buildDataloaders = require('./dataloaders');
const formatError = require('./formatError');
const connectMongo = require('./mongo-connector');
const {SubscriptionServer} = require('subscriptions-transport-ws');
const {createServer} = require('http');
const depthLimit = require('graphql-depth-limit');
const PORT = process.env.PORT || 3000;
const DEPTH_LIMIT = 3;


const start = async () => {
    const mongo = await connectMongo();
    const buildOptions = async (req, res) => {
        return {
            formatError: formatError,
            schema: schema,
            validationRules: [ depthLimit(DEPTH_LIMIT) ],
            context: {
                dataloaders: buildDataloaders(),
                mongo
            }
        };
    };

    const app = express();
    app.use('/static', express.static('public'));
    app.use('/graphql', bodyParser.json(), graphqlExpress(buildOptions));
    app.use('/syncApi', require('./SyncApi'));
    app.use('/', graphiqlExpress({endpointURL: '/graphql',subscriptionsEndpoint: `ws://localhost:${PORT}/subscriptions`}));

    const server = createServer(app);
    server.listen(PORT, () => {
        SubscriptionServer.create(
            {execute, subscribe, schema},
            {server, path: '/subscriptions'},
        );
        console.log(`Studio ghibli GraphQL server running on port ${PORT}.`);
    });
};

start();