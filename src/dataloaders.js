const DataLoader = require('dataloader');
const reqPromise = require('request-promise');

async function batchApiRequest(urls) {
    return await Promise.all(urls.map((url) => reqPromise({uri: url, json: true})));
}

module.exports = () => ({
    userLoader: new DataLoader(
        keys => batchUsers(Users, keys),
        {cacheKeyFn: key => key.toString()},
    ),
    peopleLoader: new DataLoader(
        url => batchApiRequest(url),
        {cacheKeyFn: key => key.toString()},
    ),
    filmLoader: new DataLoader(
        url => batchApiRequest(url),
        {cacheKeyFn: key => key.toString()},
    ),
    specieLoader: new DataLoader(
        url => batchApiRequest(url),
        {cacheKeyFn: key => key.toString()},
    ),
    vehicleLoader: new DataLoader(
        url => batchApiRequest(url),
        {cacheKeyFn: key => key.toString()},
    ),
    locationoader: new DataLoader(
        url => batchApiRequest(url),
        {cacheKeyFn: key => key.toString()},
    ),
});