const uuidv4 = require('uuid/v4');
const pubsub = require('../pubsub');

module.exports = {
        create: async (pubishTriggerName , mongoCollection, input) => {
            const newItem = Object.assign({_id: uuidv4()}, input);
            await mongoCollection.insert(newItem);

            let payload = {};
            payload[pubishTriggerName] = {mutation: 'CREATED', node: newItem};

            pubsub.publish(pubishTriggerName, payload);
            return newItem;
        },
        update: async (pubishTriggerName , mongoCollection, input, id) => {
            const updateItem = Object.assign(input);
            await mongoCollection.update({_id: id}, updateItem);
            updateItem._id = id;

            let payload = {};
            payload[pubishTriggerName] = {mutation: 'UPDATED', node: updateItem};

            pubsub.publish(pubishTriggerName, payload);
            return updateItem;
        },
        delete: async (pubishTriggerName , mongoCollection, id) => {
            const deleteItem = mongoCollection.findOne({_id: id});
            await mongoCollection.remove({_id: id});

            let payload = {};
            payload[pubishTriggerName] = {mutation: 'DELETED', node: deleteItem};

            pubsub.publish(pubishTriggerName, payload);
            return deleteItem;
        }
};