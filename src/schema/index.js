const {makeExecutableSchema} = require('graphql-tools');
const resolvers = require('./resolvers');
const {readFileSync} = require('fs');

typeDefs = readFileSync('./src/schema/index.graphql', 'utf8');
module.exports = makeExecutableSchema({typeDefs, resolvers});