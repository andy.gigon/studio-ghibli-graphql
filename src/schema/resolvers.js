const pubsub = require('../pubsub');
const DEFAULT_LIMIT = Number.MAX_SAFE_INTEGER;
const DEFAULT_SKIP = 0;
const _ = require('lodash');
const mutation = require('./mutations');
const PUBISH_TRIGGER_NAME_FILM = 'Film';
const PUBISH_TRIGGER_NAME_PEOPLE = 'People';
const PUBISH_TRIGGER_NAME_SPECIE = 'Specie';
const PUBISH_TRIGGER_NAME_VEHICLE = 'Vehicle';
const PUBISH_TRIGGER_NAME_LOCATION = 'Location';


module.exports = {
    Mutation: {
        createFilm: async (root, {input}, {mongo: {Films}}) => {
            return await mutation.create(PUBISH_TRIGGER_NAME_FILM, Films, input);
        },
        updateFilm: async (root, {id, input}, {mongo: {Films}}) => {
            return await mutation.update(PUBISH_TRIGGER_NAME_FILM, Films, input, id);
        },
        deleteFilm: async (root, {id}, {mongo: {Films}}) => {
            return await mutation.delete(PUBISH_TRIGGER_NAME_FILM, Films, id);
        },
        createPeople: async (root, {input}, {mongo: {Peoples}}) => {
            return await mutation.create(PUBISH_TRIGGER_NAME_PEOPLE, Peoples, input);
        },
        updatePeople: async (root, {id, input}, {mongo: {Peoples}}) => {
            return await mutation.update(PUBISH_TRIGGER_NAME_PEOPLE, Peoples, input, id);
        },
        deletePeople: async (root, {id}, {mongo: {Species}}) => {
            return await mutation.delete(PUBISH_TRIGGER_NAME_PEOPLE, Species, id);
        },
        createSpecie: async (root, {input}, {mongo: {Species}}) => {
            return await mutation.create(PUBISH_TRIGGER_NAME_SPECIE, Species, input);
        },
        updateSpecie: async (root, {id, input}, {mongo: {Species}}) => {
            return await mutation.update(PUBISH_TRIGGER_NAME_SPECIE, Species, input, id);
        },
        deleteSpecie: async (root, {id}, {mongo: {Species}}) => {
            return await mutation.delete(PUBISH_TRIGGER_NAME_SPECIE, Species, id);
        },
        createVehicle: async (root, {input}, {mongo: {Vehicles}}) => {
            return await mutation.create(PUBISH_TRIGGER_NAME_VEHICLE, Vehicles, input);
        },
        updateVehicle: async (root, {id, input}, {mongo: {Vehicles}}) => {
            return await mutation.update(PUBISH_TRIGGER_NAME_VEHICLE, Vehicles, input, id);
        },
        deleteVehicle: async (root, {id}, {mongo: {Vehicles}}) => {
            return await mutation.delete(PUBISH_TRIGGER_NAME_VEHICLE, Vehicles, id);
        },
        createLocation: async (root, {input}, {mongo: {Locations}}) => {
            return await mutation.create(PUBISH_TRIGGER_NAME_LOCATION, Locations, input);
        },
        updateLocation: async (root, {id, input}, {mongo: {Locations}}) => {
            return await mutation.update(PUBISH_TRIGGER_NAME_LOCATION, Locations, input, id);
        },
        deleteLocation: async (root, {id}, {mongo: {Locations}}) => {
            return await mutation.delete(PUBISH_TRIGGER_NAME_LOCATION, Locations, id);
        },
    },
    Subscription: {
        Film: {
            subscribe: () => pubsub.asyncIterator(PUBISH_TRIGGER_NAME_FILM),
        },
        People: {
            subscribe: () => pubsub.asyncIterator(PUBISH_TRIGGER_NAME_PEOPLE),
        },
        Specie: {
            subscribe: () => pubsub.asyncIterator(PUBISH_TRIGGER_NAME_SPECIE),
        },
        Vehicle: {
            subscribe: () => pubsub.asyncIterator(PUBISH_TRIGGER_NAME_VEHICLE),
        },
        Location: {
            subscribe: () => pubsub.asyncIterator(PUBISH_TRIGGER_NAME_LOCATION),
        },
    },
    Query: {
        films: async (root, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Films}}) => {
            return await Films.find().limit(limit).skip(skip).toArray();
        },
        film: async (root, {id}, {mongo: {Films}}) => {
            return await Films.findOne({_id: id});
        },
        peoples: async (root, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Peoples}}) => {
            return await Peoples.find().limit(limit).skip(skip).toArray();
        },
        people: async (root, {id}, {mongo: {Peoples}}) => {
            return await Peoples.findOne({_id: id});
        },
        species: async (root, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Species}}) => {
            return await Species.find().limit(limit).skip(skip).toArray();
        },
        specie: async (root, {id}, {mongo: {Species}}) => {
            return await Species.findOne({_id: id});
        },
        vehicles: async (root, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Vehicles}}) => {
            return await Vehicles.find().limit(limit).skip(skip).toArray();
        },
        vehicle: async (root, {id}, {mongo: {Vehicles}}) => {
            return await Vehicles.findOne({_id: id});
        },
        locations: async (root, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Locations}}) => {
            return await Locations.find().limit(limit).skip(skip).toArray();
        },
        location: async (root, {id}, {mongo: {Locations}}) => {
            return await Locations.findOne({_id: id});
        },
    },
    Film: {
        id: root => root._id || root.id,
        picture_url: ({_id, picture_url}) => picture_url ? picture_url : 'https://ghibligraphql.herokuapp.com/static/images/films/' + _id  + '.jpg',
        peoples: async ({people}, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Peoples}}) => {
            return _.isUndefined(people) ? [] : await Promise.all((people.slice(skip, skip + limit)).map(id => Peoples.findOne({_id: id})));
        },
        species: async ({species}, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Species}}) => {
            return _.isUndefined(species) ? [] : await Promise.all((species.slice(skip, skip + limit)).map(id => Species.findOne({_id: id})));
        },
        vehicles: async ({vehicles}, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Vehicles}}) => {
            return _.isUndefined(vehicles) ? [] : await Promise.all((vehicles.slice(skip, skip + limit)).map(id => Vehicles.findOne({_id: id})));
        },
        locations: async ({locations}, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Locations}}) => {
            return _.isUndefined(locations) ? [] : await Promise.all((locations.slice(skip, skip + limit)).map(id => Locations.findOne({_id: id})));
        }
    },
    People: {
        id: root => root._id || root.id,
        picture_url: ({_id, picture_url}) => picture_url ? picture_url : '/static/images/peoples/' + _id  + '.jpg',
        films: async ({films}, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Films}}) => {
            return _.isUndefined(films) ? [] : await Promise.all((films.slice(skip, skip + limit)).map(id => Films.findOne({_id: id})));
        },
        specie: async ({species}, data, {mongo: {Species}}) => {
            return await Species.findOne({_id: species});
        },
    },
    Specie: {
        id: root => root._id || root.id,
        picture_url: ({_id, picture_url}) => picture_url ? picture_url : '/static/images/species/' + _id  + '.jpg',
        films: async ({films}, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Films}}) => {
            return _.isUndefined(films) ? [] : await Promise.all((films.slice(skip, skip + limit)).map(id => Films.findOne({_id: id})));
        },
        peoples: async ({people}, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Peoples}}) => {
            return _.isUndefined(people) ? [] : await Promise.all((people.slice(skip, skip + limit)).map(id => Peoples.findOne({_id: id})));
        },
    },
    Vehicle: {
        id: root => root._id || root.id,
        picture_url: ({_id, picture_url}) => picture_url ? picture_url : '/static/images/vehicles/' + _id  + '.jpg',
        pilot: async ({pilot}, data, {mongo: {Peoples}}) => {
            return await Peoples.findOne({_id: pilot});
        },
        film: async ({films}, data, {mongo: {Films}}) => {
            return await Films.findOne({_id: films});
        },
    },
    Location: {
        id: root => root._id || root.id,
        picture_url: ({_id, picture_url}) => picture_url ? picture_url : '/static/images/locations/' + _id  + '.jpg',
        films: async ({films}, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Films}}) => {
            return _.isUndefined(films) ? [] : await Promise.all((films.slice(skip, skip + limit)).map(id => Films.findOne({_id: id})));
        },
        residents: async ({residents}, {limit = DEFAULT_LIMIT, skip = DEFAULT_SKIP}, {mongo: {Peoples}}) => {
            return _.isUndefined(residents) ? [] : await Promise.all((residents.slice(skip, skip + limit)).map(id => Peoples.findOne({_id: id})));
        },

    }
};