const {Logger, MongoClient} = require('mongodb');

// 1
const MONGO_URL = process.env.MONGODB_URI || 'mongodb://localhost:27017/ghibli';

// 2
module.exports = async () => {
    const db = await MongoClient.connect(MONGO_URL);

    let logCount = 0;
    Logger.setCurrentLogger((msg, state) => {
        console.log(`MONGO DB REQUEST ${++logCount}: ${msg}`);
    });
    Logger.setLevel('debug');
    Logger.filter('class', ['Cursor']);


    return {
        Films: db.collection('films'),
        Peoples: db.collection('peoples'),
        Species: db.collection('species'),
        Vehicles: db.collection('vehicles'),
        Locations: db.collection('locations')
    };
}
