const REXEXP_UUID = /[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}/;
const reqPromise = require('request-promise');
const validUrl = require('valid-url');
const connectMongo = require('./../mongo-connector');

module.exports = function (req, res) {
    let sync = {
        Films: "https://ghibliapi.herokuapp.com/films",
        Peoples: "https://ghibliapi.herokuapp.com/people",
        Species: "https://ghibliapi.herokuapp.com/species",
        Vehicles: "https://ghibliapi.herokuapp.com/vehicles",
        Locations: "https://ghibliapi.herokuapp.com/locations",
    };
    let promises = [];
    function getUuidbyUrl(url) {
        return REXEXP_UUID.test(url) ? REXEXP_UUID.exec(url)[0] : null;
    }
    async function syncMongoCollection(mongoCollectionName, url) {
        const mongo = await connectMongo();
        let items = await reqPromise({uri: url, json: true});
        items.forEach(async (newItem) => {
            newItem.picture_url = '';
            newItem._id = Object.assign(newItem).id;
            delete newItem.id;
            delete newItem.url;
            Object.keys(newItem).map(function (key) {
                if (Array.isArray(newItem[key])) {
                    newItem[key] = newItem[key].filter(url => REXEXP_UUID.test(url)).map(url => getUuidbyUrl(url));
                } else if (validUrl.isUri(newItem[key])) {
                    newItem[key] = getUuidbyUrl(newItem[key])
                }
            });
            await mongo[mongoCollectionName].update({_id: newItem._id}, newItem, {upsert: true});
        })
    }
    for (let mongoCollectionName in sync) {
        console.log(mongoCollectionName + " : " + sync[mongoCollectionName]);
        promises.push(syncMongoCollection(mongoCollectionName, sync[mongoCollectionName]));
    }
    Promise.all(promises)
        .then(results => res.status(201).send('Sync OK'))
        .catch(err => res.status(500).send({ error: 'Something failed!' }));
};